package me.github.fwfurtado.ipay.user.api.listing;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class ListingContractTest {

    private static final UUID EXISTING_USER_ID = UUID.fromString("9925923a-43b0-41f0-a897-89dcbfd790a9");
    private static final UUID NOT_EXISTING_USER_ID = UUID.fromString("30806ec1-0e77-426f-9d05-9ed8fabd64c2");
    private static final UserView EXISTING_USER = () -> List.of("fernando.furtado@caelum.com.br");

    @Mock(lenient = true)
    private ListingRepository repository;


    @BeforeEach
    void setup() {
        given(repository.loadUserById(EXISTING_USER_ID)).willReturn(Optional.of(EXISTING_USER));
        given(repository.loadUserById(NOT_EXISTING_USER_ID)).willReturn(Optional.empty());

        RestAssuredMockMvc.standaloneSetup(new ListingController(repository));
    }

}