package me.github.fwfurtado.ipay.user.api.creation;

import me.github.fwfurtado.ipay.user.UserApplication;
import me.github.fwfurtado.ipay.user.shared.domains.User;
import me.github.fwfurtado.ipay.user.shared.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.will;
import static org.mockito.BDDMockito.willAnswer;
import static org.mockito.Mockito.doAnswer;

@SpringBootTest(classes = UserApplication.class)
@AutoConfigureMessageVerifier
public class CreationContractTest {

    @MockBean
    private UserRepository repositry;

    @Autowired
    private CreationService service;


    @BeforeEach
    void setup() {
        given(repositry.existsByEmailsContains(anyString())).willReturn(false);
        will(setUserId()).given(repositry).save(any(User.class));
    }

    private Answer<?> setUserId() {
        return invocation -> {
            var user = (User) invocation.getArgument(0);

            ReflectionTestUtils.setField(user, "id", UUID.fromString("30806ec1-0e77-426f-9d05-9ed8fabd64c2"));
            return null;
        };
    }

    public void fireEvent() {
        var form = new CreationController.CreationUserForm();
        form.setEmail("fwfurtado@gmail.com");
        form.setPassword("12345");

        service.createNewUserBy(form);
    }
}
