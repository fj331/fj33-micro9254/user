import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        urlPath('/users/30806ec1-0e77-426f-9d05-9ed8fabd64c2' ) {
            queryParameters {
                parameter("pages", 1)
            }
        }
        method GET()
        headers {
            accept applicationJson()
        }
    }

    response {
        status NOT_FOUND()
    }
}