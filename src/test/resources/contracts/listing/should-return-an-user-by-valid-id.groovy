import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        url('/users/9925923a-43b0-41f0-a897-89dcbfd790a9?pages=1')

        method GET()

        headers {
            accept applicationJson()
        }
    }

    response {
        status OK()

        headers {
            contentType applicationJson()
        }

        body(
            "emails": [
                    "fernando.furtado@caelum.com.br"
            ]
        )

    }
}