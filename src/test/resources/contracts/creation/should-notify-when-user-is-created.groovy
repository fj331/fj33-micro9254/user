import org.springframework.cloud.contract.spec.Contract

Contract.make {

    input {
        label 'new-user'
        triggeredBy('fireEvent()')
    }

    outputMessage {

        sentTo('user')

        body(
                "id": "30806ec1-0e77-426f-9d05-9ed8fabd64c2",
                "emails": [
                        "fwfurtado@gmail.com"
                ]
        )
    }

}