package me.github.fwfurtado.ipay.user.api.wallets;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@AllArgsConstructor
class ShowWalletsController {


    private final ShowWalletsService service;

    @GetMapping("users/{userId}/wallets")
    ResponseEntity<?> showWallets(@PathVariable UUID userId) {
        return service.showWalletsOfUser(userId).map(ok()::body).orElseGet(notFound()::build);
    }
}
