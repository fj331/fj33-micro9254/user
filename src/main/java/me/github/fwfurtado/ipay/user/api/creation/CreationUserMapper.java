package me.github.fwfurtado.ipay.user.api.creation;

import me.github.fwfurtado.ipay.user.api.creation.CreationController.CreationUserForm;
import me.github.fwfurtado.ipay.user.shared.domains.User;
import me.github.fwfurtado.ipay.user.shared.infra.Mapper;
import org.springframework.stereotype.Component;

@Component
class CreationUserMapper implements Mapper<CreationUserForm, User> {
    @Override
    public User map(CreationUserForm source) {
        return new User(source.getEmail(), source.getPassword());
    }
}
