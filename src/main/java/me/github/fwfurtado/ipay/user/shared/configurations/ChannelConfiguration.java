package me.github.fwfurtado.ipay.user.shared.configurations;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableBinding(ChannelConfiguration.UserChannel.class)
public class ChannelConfiguration {


    public interface UserChannel {
        @Output("user")
        MessageChannel channel();
    }
}
