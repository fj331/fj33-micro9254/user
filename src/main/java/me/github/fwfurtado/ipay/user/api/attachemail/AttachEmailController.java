package me.github.fwfurtado.ipay.user.api.attachemail;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.accepted;


@RestController
@AllArgsConstructor
public class AttachEmailController implements AddNewEmailController {

    private final AttachEmailService service;

    @Override
    @PutMapping("users/{id}")
    public ResponseEntity<?> addEmailBy(@PathVariable UUID id, @RequestBody @Valid AddEmailForm form) {
        form.setUserId(id);

        service.addEmailBy(form);

        return accepted().build();
    }

    @Data
    static class AddEmailForm {
        @JsonIgnore
        private UUID userId;

        @NotBlank
        @Email
        private String email;
    }
}
