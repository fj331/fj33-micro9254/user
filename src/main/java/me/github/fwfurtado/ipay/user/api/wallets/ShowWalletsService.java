package me.github.fwfurtado.ipay.user.api.wallets;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.AllArgsConstructor;
import me.github.fwfurtado.ipay.user.api.attachemail.FindUserByIdService;
import me.github.fwfurtado.ipay.user.shared.exceptions.UserNotFoundException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
class ShowWalletsService {

    private final FindUserByIdService findUserIdService;
    private final FindWalletsService findWalletsService;

    @Retryable(maxAttempts = 5, backoff = @Backoff(delay = 2000, multiplier = 2.0, maxDelay = 5000))
    public Optional<WalletView> showWalletsOfUser(UUID userId) {

        var userNotFound = findUserIdService.findById(userId).isEmpty();

        if (userNotFound) {
            throw new UserNotFoundException(MessageFormat.format("Cannot find user with id {0}", userId));
        }

        return findWalletsService.findMyWallets(userId);
    }

    @Recover
    public Optional<WalletView> recover(UUID userId) {
        System.out.println("Cannot find user with id " + userId);

        return Optional.empty();
    }
}
