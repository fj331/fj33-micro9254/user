package me.github.fwfurtado.ipay.user.api.listing;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ipay.user.api.attachemail.AttachEmailController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Links;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.afford;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@AllArgsConstructor
class ListingController {

    private final ListingRepository repository;

    @GetMapping("users/{id}")
    ResponseEntity<EntityModel<UserView>> showUserBy(@PathVariable UUID id, @RequestParam Long pages) {

        System.out.println("REQUEST");

        var links = Links.of(
                linkTo(methodOn(ListingController.class).showUserBy(id, null)).withSelfRel()
                    .andAffordance(afford(methodOn(AttachEmailController.class).addEmailBy(id, null)))
        );


        return repository.loadUserById(id)
                .map( user -> EntityModel.of(user, links))
                .map(ok()::body).orElseGet(notFound()::build);
    }

    @GetMapping("filter")
    ResponseEntity<UserAndPasswordView> filterUser(@RequestParam String email) {
        return  repository.loadUserByEmail(email)
                .map(ok()::body).orElseGet(notFound()::build);
    }

}
