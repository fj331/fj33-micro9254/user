package me.github.fwfurtado.ipay.user.api.attachemail;

import me.github.fwfurtado.ipay.user.shared.domains.User;

import java.util.Optional;
import java.util.UUID;

public interface FindUserByIdService {
    Optional<User> findById(UUID userId);
}
