package me.github.fwfurtado.ipay.user.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.github.fwfurtado.ipay.user.shared.exceptions.UserAlreadyExistsException;
import me.github.fwfurtado.ipay.user.shared.exceptions.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(UserAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    ErrorView handle(UserAlreadyExistsException e) {
        var message = e.getMessage();
        log.error("[EXCEPTION] [DUPLICATED_USER] {}", message);

        return new ErrorView(message);
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorView handle(UserNotFoundException e) {
        var message = e.getMessage();

        log.error("[EXCEPTION] [USER_NOT_FOUND] {}", message);

        return new ErrorView(message);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorView handle(MethodArgumentNotValidException e) {
        var bindingResult = e.getBindingResult();

        log.error("[EXCEPTION] [VALIDATION_ERROR] {}", bindingResult);

        var globalErrors = bindingResult.getGlobalErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .map(GlobalErrorEntry::new);


        var fieldErrors = bindingResult.getFieldErrors().stream()
                .collect(toMap(FieldError::getField, FieldError::getDefaultMessage))
                .entrySet().stream()
                .map(keyValue(FieldErrorEntry::new));

        var allErrors = Stream.concat(globalErrors, fieldErrors).collect(toList());

        return new ErrorView(allErrors);
    }


    private static <K, V, R> Function<Map.Entry<K, V>, R> keyValue(BiFunction<K, V, R> mapping) {
        return entry -> mapping.apply(entry.getKey(), entry.getValue());
    }


    @Getter
    @AllArgsConstructor
    private static class ErrorView {
        private final List<GlobalErrorEntry> errors;

        public ErrorView(String message) {
            errors = List.of(new GlobalErrorEntry(message));
        }
    }


    @Getter
    @AllArgsConstructor
    private static class GlobalErrorEntry {
        private final String message;
    }

    @Getter
    private static class FieldErrorEntry extends GlobalErrorEntry {
        private final String field;

        public FieldErrorEntry(String field, String message) {
            super(message);
            this.field = field;
        }
    }
}
