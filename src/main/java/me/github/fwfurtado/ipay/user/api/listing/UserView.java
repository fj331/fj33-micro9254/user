package me.github.fwfurtado.ipay.user.api.listing;

import java.util.List;

public interface UserView {
    List<String> getEmails();
}
