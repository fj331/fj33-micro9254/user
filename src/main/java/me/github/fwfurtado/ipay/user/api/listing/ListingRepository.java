package me.github.fwfurtado.ipay.user.api.listing;

import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface ListingRepository {

    @Query("select u from User u where u.id = :id")
    Optional<UserView> loadUserById(UUID id);

    @Query("select e as email, u.password as password from User u join u.emails e where e = :email")
    Optional<UserAndPasswordView> loadUserByEmail(String email);

}
