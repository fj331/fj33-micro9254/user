package me.github.fwfurtado.ipay.user.api.attachemail;

import me.github.fwfurtado.ipay.user.shared.domains.User;

public interface AddEmailRepository {
    void save(User user);
}
