package me.github.fwfurtado.ipay.user.shared.infra;

public interface Mapper<S, T> {

    T map(S source);
}
