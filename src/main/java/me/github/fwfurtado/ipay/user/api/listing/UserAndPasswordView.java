package me.github.fwfurtado.ipay.user.api.listing;

public interface UserAndPasswordView {
    String getEmail();
    String getPassword();
}
