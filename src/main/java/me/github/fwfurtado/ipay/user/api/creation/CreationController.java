package me.github.fwfurtado.ipay.user.api.creation;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import static org.springframework.http.ResponseEntity.created;

@RestController
@AllArgsConstructor
class CreationController {

    private final CreationService service;

    @PostMapping("users")
    ResponseEntity<?> createBy(@RequestBody @Valid CreationUserForm form, UriComponentsBuilder uriBuilder) {

        var id = service.createNewUserBy(form);

        var uri = uriBuilder.path("/users/{id}").build(id);

        return created(uri).build();
    }

    @Data
    static class CreationUserForm {

        @NotBlank
        @Email
        private String email;
        
        @NotBlank
        private String password;
    }
}
