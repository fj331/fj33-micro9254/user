package me.github.fwfurtado.ipay.user.api.attachemail;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ipay.user.shared.exceptions.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
@AllArgsConstructor
class AttachEmailService {

    private final FindUserByIdService findUserService;
    private final AddEmailRepository repository;

    public void addEmailBy(AttachEmailController.AddEmailForm form) {
        var userId = form.getUserId();

        var user = findUserService.findById(userId).orElseThrow(() -> new UserNotFoundException(MessageFormat.format("Cannot find user with id {0}", userId)));

        user.addEmail(form.getEmail());

        repository.save(user);
    }
}
