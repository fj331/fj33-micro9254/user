package me.github.fwfurtado.ipay.user.shared.repositories;

import me.github.fwfurtado.ipay.user.api.attachemail.AddEmailRepository;
import me.github.fwfurtado.ipay.user.api.attachemail.FindUserByIdService;
import me.github.fwfurtado.ipay.user.api.creation.CreationRepository;
import me.github.fwfurtado.ipay.user.api.creation.ExistsUserService;
import me.github.fwfurtado.ipay.user.api.listing.ListingRepository;
import me.github.fwfurtado.ipay.user.shared.domains.User;
import org.springframework.data.repository.Repository;

import java.util.UUID;

public interface UserRepository extends Repository<User, UUID>, CreationRepository, ExistsUserService, ListingRepository, AddEmailRepository, FindUserByIdService {
}
