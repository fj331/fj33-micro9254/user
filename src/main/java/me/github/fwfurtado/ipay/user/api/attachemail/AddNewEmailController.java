package me.github.fwfurtado.ipay.user.api.attachemail;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.UUID;

public interface AddNewEmailController {
    @PutMapping("users/{id}")
    ResponseEntity<?> addEmailBy(@PathVariable UUID id, @RequestBody @Valid AttachEmailController.AddEmailForm form);
}
