package me.github.fwfurtado.ipay.user.api.wallets;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.UUID;

import static me.github.fwfurtado.ipay.user.api.wallets.FindWalletsService.FindWalletsServiceFallback;

@FeignClient(name = "wallets", fallback = FindWalletsServiceFallback.class)
public interface FindWalletsService {

    @GetMapping("wallets/me")
    Optional<WalletView> findMyWallets(@RequestParam UUID userId);

    @Component
    class FindWalletsServiceFallback implements FindWalletsService {

        @Override
        public Optional<WalletView> findMyWallets(UUID userId) {
            System.out.println("FEIGN FALLBACK");
            return Optional.empty();
        }
    }
}
