package me.github.fwfurtado.ipay.user.api.wallets;

import lombok.Data;

import java.util.List;

@Data
public class WalletView {
    private List<WalletName> wallets;

    @Data
    static class WalletName {
        private String name;
    }
}
