package me.github.fwfurtado.ipay.user.api.creation;

public interface ExistsUserService {
    boolean existsByEmailsContains(String email);
}
