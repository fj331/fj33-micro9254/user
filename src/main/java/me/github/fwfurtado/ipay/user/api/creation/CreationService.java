package me.github.fwfurtado.ipay.user.api.creation;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ipay.user.api.creation.CreationController.CreationUserForm;
import me.github.fwfurtado.ipay.user.shared.configurations.ChannelConfiguration;
import me.github.fwfurtado.ipay.user.shared.domains.User;
import me.github.fwfurtado.ipay.user.shared.exceptions.UserAlreadyExistsException;
import me.github.fwfurtado.ipay.user.shared.infra.Mapper;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.UUID;

@Service
@AllArgsConstructor
class CreationService {

    private final ExistsUserService findUsers;
    private final Mapper<CreationUserForm, User> mapper;
    private final CreationRepository repository;
    private final ChannelConfiguration.UserChannel source;

    public UUID createNewUserBy(CreationUserForm form) {
        var email = form.getEmail();

        if (findUsers.existsByEmailsContains(email)) {
            throw new UserAlreadyExistsException(MessageFormat.format("Already exist user with email {0}", email));
        }

        var user = mapper.map(form);

        repository.save(user);

        var message = MessageBuilder.withPayload(user).build();

        source.channel().send(message);

        return user.getId();
    }
}
