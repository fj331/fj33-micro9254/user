package me.github.fwfurtado.ipay.user.api.creation;

import me.github.fwfurtado.ipay.user.shared.domains.User;

public interface CreationRepository {
    void save(User user);
}
