CREATE TABLE users (
    id UUID,
    password VARCHAR(255) NOT NULL,

    CONSTRAINT pk_users PRIMARY KEY(id)
);

CREATE TABLE user_emails (
    email VARCHAR(50),
    user_id UUID NOT NULL,

    CONSTRAINT pk_user_emails PRIMARY KEY (email)
);