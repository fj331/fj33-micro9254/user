# Segurança

##  Atenticação
Formas de identificar quem está usando o sistema:

1. Username/Password
2. Biometria
3. Leitura de Iris
4. Token

## Autorização
O que pessoa pode fazer dentro do sistema

Papeis (Role)

## Criptografia (2way)
HTTPS

## Hashing/Encoding
MD5
MD6
SHA-256
SHA-512

BCrypt
Argon2

Base64

oAuth, OpenID (Open connect ID), JWT, 2Factor, Cookies/Session, OTP (one-time password)



## oAuth

##  Participantes:
1. Resource Owner (Leonardo) username/password
2. Client Application (Aplicação que quer acessar os dados usuário) client_id/client_secret
3. Resource Server (Dados do usuário)
4. Authorization Server

## Access Token

```json
{
  "token_type": "bearer",
  "access_token": "ff65591a-0b81-4c58-9d1c-e3b0529b3c7c",
  "expire_in": 123688
}
```
## Grant Types

1. Authorization Code Grant
2. Password Grant
3. Implicit Grant
4. Client Credentials
5. Refresh Token


### Password Grant
```mermaid
sequenceDiagram
  participant RO as Leonardo
  participant CA as Angular
  participant AS as Api Gateway
  participant RS as Microservices  	
  
  RO->>CA: logar
  CA->>RO: Form de login
  RO->>CA: username/password
  CA->>AS: username/password client_id/client_secret
  AS->>AS: Valida client_id/client_secret
  AS->>AS: Valida username/password
  AS->>CA: Devolve AccessToken
  CA->>RS: GET /pictures - Authorization: bearer 123
  RS->>AS: token
  AS->>AS: Valida o Token
  AS->>RS: Token OK
  RS->>CA: Pictures
  
 ```


















